package avery.lindley.foreverevents.dao;


import avery.lindley.foreverevents.model.Event;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EventsRepository extends MongoRepository<Event, String> {
}
