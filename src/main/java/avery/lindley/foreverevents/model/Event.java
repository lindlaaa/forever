package avery.lindley.foreverevents.model;


import org.springframework.data.annotation.Id;

import java.util.Date;

public class Event {

    @Id
    private String id;

    private Date _create_time;
    private String _title;
    private String _description;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date get_create_time() {
        return _create_time;
    }

    public void set_create_time(Date _create_time) {
        this._create_time = _create_time;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }
}
